import React, {Component} from 'react';
import styles from "./styles";
import {Alert, TouchableOpacity, View, Text} from "react-native";


onPress1 = () => {
    Alert.alert('Bạn đã gửi tín hiệu SOS!')
};
onPress2 = () => {
    Alert.alert('Bạn đã gửi thông báo đến cảnh sát!')
};
onPress3 = () => {
    Alert.alert('Cây xăng gần nhất sẽ được tìm thấy!')
};
onPress4 = () => {
    Alert.alert('Thông tin của bạn là:')
};

const Button_SOS = () => {
    return(
        <TouchableOpacity style={{flex:1}}
                          onPress={ () => {this.onPress1()}}>
            <View style={styles.sosSectionText}>
                <Text style={styles.styleText3}>Phát tín hiệu SOS</Text>
            </View>
        </TouchableOpacity>
    )
};

const Button_POLICE = () => {
    return(
        <TouchableOpacity style={{flex:1}}
                          onPress={ () => {this.onPress2()}}>
            <View style={styles.policeSectionText}>
                <Text style={styles.styleText3}>Báo trạm Công An</Text>
            </View>
        </TouchableOpacity>
    )
};

const Button_FIND = () => {
    return(
        <TouchableOpacity style={{flex:1}}
                          onPress={ () => {this.onPress3()}}>
            <View style={styles.findSectionText}>
                <Text style={styles.styleText3}>Tìm cây xăng</Text>
            </View>
        </TouchableOpacity>
    )
};

const Button_ACCOUNT = () => {
    return(
        <TouchableOpacity style={{flex:1}}
                          onPress={() => {this.onPress4()}}>
            <View style={styles.accountSectionText}>
                <Text style={styles.styleText3}>Tài khoản của bạn</Text>
            </View>
        </TouchableOpacity>
    )
};

export {Button_SOS,Button_POLICE,Button_FIND,Button_ACCOUNT};