import React, {Component} from 'react';
import {Dimensions} from "react-native";
import styles from "./styles";
import MapStyle from "./MapStyle";
import {PROVIDER_GOOGLE} from "react-native-maps";
import Mapview from 'react-native-maps'

let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.05;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class constructor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            marker: {
                latitude: 16.0202575,
                longitude: 108.218597,
            },
        }
    }
    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    },
                    marker: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                });
            },
            (error) => console.log(error.message),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        );
        this.watchID = navigator.geolocation.watchPosition(
            position => {
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    },
                });
            });
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }
}
const View_mapviewMarker = () => {
    return(
        <Mapview style={styles.map}
    customMapStyle={MapStyle}
    region={this.state.region}
    showsUserLocation={true}
    followsUserLocation={true}
    provider={PROVIDER_GOOGLE}
    onRegionChange={ region => this.setState({region}) }
>
<Mapview.Marker
    coordinate={this.state.marker}>
<View>
    <Image source={require('./assets/images/car_2018-09-06/car.png')}></Image>
</View>
</Mapview.Marker>
</Mapview>
    )
};
export {constructor, View_mapviewMarker};

