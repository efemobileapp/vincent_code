import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    styleText1: {
        flex:1,
        marginBottom: 20,
        marginLeft: 20,
        justifyContent:'flex-end',
    },
    Text1:{
        fontSize: 15,
        color:'#a6b0c1',
    },
    Text2:{
        fontSize: 24,
        color:'white',
        fontWeight:'bold',
    },
    Text3:{
        fontSize: 18,
        color:'white',
    },
    general: {
        flex:1,
        flexDirection:'column',
        marginLeft:20,
        marginRight:20,
        borderRadius: 18,
        backgroundColor: 'white',
        marginBottom:20,
    },
    sectionRow1: {
        flex: 2,
        borderBottomColor: '#e1e7f2',
        borderBottomWidth: 1.5,
        flexDirection: 'row',
        shadowColor:'#ee9bef',
        shadowOffset:{width:10, height:10},
        backgroundColor:'white',
        borderTopLeftRadius: 18,
        borderTopRightRadius:18,
    },
    Speed: {
        flex: 1,
        borderRightColor: 'white',
        borderRightWidth: 1,
        justifyContent:'center',
    },
    greenIMG:{
        marginLeft:15,
        resizeMode: 'contain',
        width: 50,
        height: 50,
    },
    numberSpeed:{
        fontSize: 26,
        color:'green',
        fontWeight:'bold',
        marginLeft:15,
    },
    textSpeed : {
        fontSize: 14,
    },
    greenTextSpeed:{
        fontSize: 14,
        color:'green',
    },
    redSpeed:{
        flex:1,
        borderRightWidth: 1,
        borderRightColor:'white',
        borderTopRightRadius:18,
        justifyContent:'center',
    },
    redIMG:{
        marginLeft:15,
        resizeMode: 'contain',
        width: 52,
        height: 52,
    },
    redTextSpeed:{
        fontSize: 14,
        color:'red',
    },
    styleText2: {
        fontSize: 13,
        fontWeight: 'bold',
        marginLeft: 13
    },
    sectionRow2: {
        flex: 1,
        borderBottomColor: '#e1e7f2',
        borderBottomWidth: 1.5,
        flexDirection: 'row',
        backgroundColor:'#f2f6fc',
    },
    sosSectionIMG: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        justifyContent:'center',
        alignItems:'center',
    },
    sosIMG:{
        width: 50,
        height: 50,
        resizeMode: 'center',
    },
    sosSectionText: {
        flex: 1,
        borderRightColor: '#e1e7f2',
        borderRightWidth: 1,
        justifyContent:'center',
        alignItems:'center',
    },
    policeSectionIMG: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        justifyContent:'center',
        alignItems:'center',
    },
    policeIMG:{
        width: 55,
        height: 60,
        resizeMode: 'center',
    },
    policeSectionText: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'white',
        justifyContent:'center',
    },
    sectionRow3: {
        flex: 1,
        borderBottomColor: '#f2f6fc',
        borderBottomWidth: 1.5,
        flexDirection: 'row',
        backgroundColor:'#f2f6fc',
        borderBottomLeftRadius: 18,
        borderBottomRightRadius:18,
    },
    findSectionIMG: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        justifyContent:'center',
        alignItems:'center',
    },
    findIMG:{
        width:45,
        height:45,
        resizeMode:'center'
    },
    findSectionText: {
        flex: 1,
        borderRightColor: '#e1e7f2',
        borderRightWidth: 1,
        justifyContent:'center',
        alignItems:'center',
    },
    accountSectionIMG: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        justifyContent:'center',
        alignItems:'center',
    },
    accountIMG:{
        width: 60,
        height: 65,
        resizeMode: 'center',
    },
    accountSectionText: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        borderBottomRightRadius:18,
        justifyContent:'center',
        alignItems:'center',
    },
    styleText3:{
        fontSize: 16,
        fontWeight:'bold',
    },

    radius:{
        height:50,
        width:50,
        borderRadius:50/2,
        overflow:'hidden',
        backgroundColor:'rgba(0, 122, 255, 0.1)',
        borderWidth:1,
        borderColor:'rgba(0, 112, 255, 0.3)',
        alignItems:'center',
        justifyContent:'center',
    },
    dot:{
        height:20,
        width:20,
        borderWidth:3,
        borderColor:'white',
        borderRadius:20/2,
        overflow:'hidden',
        backgroundColor:'#007AFF',
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
});

export default styles;