import React, { Component } from "react";
import { AppRegistry, StyleSheet, Dimensions, View } from "react-native";
import { TabNavigator } from "react-navigation";
import { Container, Text } from "native-base";


class LocationA extends Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: 16.0307803,
            longitude: 108.2133133,
            error:null,
        };
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000 },
        );
    }


    render() {
        return (
            <View style={styles.container}>
                <Text> {this.state.latitude} </Text>
                <Text> {this.state.longitude} </Text>
                <Text> {this.state.error} </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
});

export default LocationA;