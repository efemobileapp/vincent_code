import React, {Component} from 'react';
import {StyleSheet, Text, Image, View} from 'react-native';
import Mapview, {PROVIDER_GOOGLE} from 'react-native-maps';
import * as ToastAndroid from 'react-native/Libraries/Components/ToastAndroid/ToastAndroid.android';

export default class Test3 extends Component{

    constructor(props){
        super(props);
        this.state={
            region:{
                latitude:16.042444,   // Cái này sẽ là tọa độ view khi mới khởi động maps, không phải tọa độ đang đứng
                longitude:108.217581, // Cái này sẽ là tọa độ view khi mới khởi động maps, không phải tọa độ đang đứng
                latitudeDelta:0.01,
                longitudeDelta:0.01},
            marker:{
                latitude:16.0202575,
                longitude:108.218597,},
        }
    }

    componentWillMount = () => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    region:{
                        latitude:position.coords.latitude,
                        longitude:position.coords.longitude,
                        latitudeDelta:0.01,
                        longitudeDelta:0.01,
                    },
                    marker:{
                        latitude:position.coords.latitude,
                        longitude:position.coords.longitude,
                    }
                })
            },
            (error) => alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };
    render(){
        return(
            <View style={styles.container}>

                <Mapview style={styles.map}
                         customMapStyle={MapStyle}
                         initialRegion={this.state.region}
                         showsUserLocation={true}
                         provider={PROVIDER_GOOGLE}>
                    <Mapview.Marker
                        coordinate={this.state.marker}>
                        <View style={styles.radius}>
                            <View style={styles.dot}/>
                        </View>
                    </Mapview.Marker>
                </Mapview>

                <View style={{ marginBottom:20, marginLeft:20}}>
                    <Text style={{fontSize: 15, color:'#a6b0c1'}}>Vị trí hiện tại  </Text>
                    <Text style={{fontSize: 24, color:'white', fontWeight:'bold'}}>TP. Đà Nẵng </Text>
                    <Text style={{fontSize: 18, color:'white'}}>Đường Lương Nhữ Hộc </Text>
                </View>

                <View style={styles.general}>
                    <View style={styles.row1}>
                        <View style={styles.column}>
                            <Image style={{marginLeft:15,resizeMode: 'center', width: 60, height: 60}}
                                   source={require('./Img/green-speed_2018-09-06/green-speed.png')}></Image>
                            <Text style={{fontSize: 26, color:'green', fontWeight:'bold', marginLeft:15}}>65
                                <Text style={{fontSize: 14, color:'green'}}> KM/H</Text></Text>
                            <Text style={{fontSize: 13, fontWeight:'bold', marginLeft:13}}> Tốc độ hiện tại</Text>
                        </View>
                        <View style={styles.column4}>
                            <Image style={{marginLeft:15,resizeMode: 'center', width: 60, height: 60}}
                                   source={require('./Img/red-one_2018-09-06/red-one.png')}></Image>
                            <Text style={{fontSize: 26, color:'red', fontWeight:'bold', marginLeft:15}}>90
                                <Text style={{fontSize: 14, color:'red'}}> KM/H</Text></Text>
                            <Text style={{fontSize: 13, fontWeight:'bold', marginLeft:13}}> Tốc độ cho phép</Text>
                        </View>
                    </View>

                    <View style={styles.row2}>
                        <View style={styles.column3}>
                            <Image style={{width: 50, height: 50, resizeMode: 'center',}}
                                   source={require('./Img/mess/icon.png')}></Image>
                        </View>
                        <View style={styles.column1}>
                            <Text style={{fontSize: 16, fontWeight:'bold'}}>Phát tín hiệu SOS</Text>
                        </View>
                        <View style={styles.column3}>
                            <Image style={{width: 55, height: 60, resizeMode: 'center',}}
                                   source={require('./Img/police/icon.png')}></Image>
                        </View>
                        <View style={styles.column4}>
                            <Text style={{fontSize: 16, fontWeight:'bold'}}>Báo trạm Công An</Text>
                        </View>
                    </View>

                    <View style={styles.row3}>
                        <View style={styles.column3}>
                            <Image style={{width:45, height:45,}}
                                   source={{uri:'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/compass-icon.png'}}></Image>
                        </View>
                        <View style={styles.column1}>
                            <Text style={{fontSize: 16, fontWeight:'bold'}}>Tìm cây xăng</Text>
                        </View>
                        <View style={styles.column3}>
                            <Image style={{width: 60, height: 65, resizeMode: 'center',}}
                                   source={require('./Img/human/icon.png')}></Image>
                        </View>
                        <View style={styles.column5}>
                            <Text style={{fontSize: 16, fontWeight:'bold'}}>Tài khoản của bạn</Text>
                        </View>
                    </View>

                </View>
            </View>
        )
    }
}

// Set MapStyle để thay đổi style bản đồ, lưu ý phải có style mặc định của map thì mới thay đổi style của map đươc, xem dòng 46,47
const MapStyle =[{
    "elementType": "geometry",
    "stylers": [
        {
            "color": "#242f3e"
        }
    ]
},
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#263c3f"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#6b9a76"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#38414e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#212a37"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9ca5b3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#1f2835"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#f3d19c"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#2f3948"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#515c6d"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    }]
const styles = StyleSheet.create({
    // Khai bao bang view chung
    general: {
        flex:0.5,
        flexDirection:'column',
        marginLeft:20,
        marginRight:20,
        borderRadius: 18,
        backgroundColor: 'white',
        marginBottom:20,},
    // Khai bao cot
    column: {
        flex: 1,
        borderRightColor: 'white',
        borderRightWidth: 1,
        // alignItems:'center',
        justifyContent:'center',
    },
    column1: {
        flex: 1,
        // backgroundColor:'blue',
        borderRightColor: '#e1e7f2',
        borderRightWidth: 1,
        justifyContent:'center',
        alignItems:'center',},
    column2: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        alignItems:'center',
        justifyContent:'center',
    },
    column3: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        justifyContent:'center',
        alignItems:'center',},
    column4: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'white',
        borderTopRightRadius:18,
        justifyContent:'center',},
    column5: {
        flex: 1,
        borderRightWidth: 1,
        borderRightColor:'#f2f6fc',
        borderBottomRightRadius:18,
        justifyContent:'center',
        alignItems:'center',},

    // Khai bao dong
    row1: {
        flex: 2,
        borderBottomColor: '#e1e7f2',
        borderBottomWidth: 1.5,
        flexDirection: 'row',
        shadowColor:'#ee9bef',
        shadowOffset:{width:10, height:10},
        backgroundColor:'white',
        borderTopLeftRadius: 18,
        borderTopRightRadius:18,},
    row2: {
        flex: 1,
        borderBottomColor: '#e1e7f2',
        borderBottomWidth: 1.5,
        flexDirection: 'row',
        backgroundColor:'#f2f6fc',},
    row3: {
        flex: 1,
        borderBottomColor: '#f2f6fc',
        borderBottomWidth: 1.5,
        flexDirection: 'row',
        backgroundColor:'#f2f6fc',
        borderBottomLeftRadius: 18,
        borderBottomRightRadius:18,},
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'flex-end',
    },
    map: {...StyleSheet.absoluteFillObject},
    radius:{
        height:50,
        width:50,
        borderRadius:50/2,
        overflow:'hidden',
        backgroundColor:'rgba(0, 122, 255, 0.1)',
        borderWidth:1,
        borderColor:'rgba(0, 112, 255, 0.3)',
        alignItems:'center',
        justifyContent:'center',
    },
    dot:{
        height:20,
        width:20,
        borderWidth:3,
        borderColor:'white',
        borderRadius:20/2,
        overflow:'hidden',
        backgroundColor:'#007AFF'
    },
})