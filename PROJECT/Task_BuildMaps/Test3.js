import React, {Component} from 'react';
import {Alert, Text, Image, View, Dimensions, TouchableOpacity} from 'react-native';
import Mapview, {PROVIDER_GOOGLE} from 'react-native-maps';
import ActionButton from 'react-native-action-button';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './src/styles';
import MapStyle from './src/MapStyle';

let { width, height } = Dimensions.get('window');
const LATITUDE = 16.036872;
const LONGITUDE = 108.210029;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;
export default class Test5 extends Component{

    constructor(props){
        super(props);
        this.state={
            region:{
                latitude:LATITUDE,
                longitude:LONGITUDE,
                latitudeDelta:LATITUDE_DELTA,
                longitudeDelta:LONGITUDE_DELTA
            },
            marker:{
                latitude:16.0202575,
                longitude:108.218597,
            },
        }
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    },
                    marker:{
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                });
            },
            (error) => console.log(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
        this.watchID = navigator.geolocation.watchPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
        );
    }
    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);}

    onPress(){
    }
    onPress1(){
        Alert.alert('Bạn đã gửi tín hiệu SOS!')
    }
    onPress2(){
        Alert.alert('Bạn đã gửi thông báo đến cảnh sát!')
    }
    onPress3(){
        Alert.alert('Cây xăng gần nhất sẽ được tìm thấy!')
    }
    onPress4(){
        Alert.alert('Thông tin của bạn là:')
    }

    render(){
        return(
            <View style={styles.container}>
                <Mapview style={styles.map}
                         customMapStyle={MapStyle}
                         initialRegion={this.state.region}
                         showsUserLocation={true}
                         followsUserLocation={true}
                         provider={PROVIDER_GOOGLE}
                         onRegionChange={ region => this.setState({region}) }
                         onRegionChangeComplete={ region => this.setState({region}) }
                >
                    <Mapview.Marker
                        coordinate={this.state.region}>
                        <View>
                            <Image source={require('./src/assets/images/car_2018-09-06/car.png')}></Image>
                        </View>
                    </Mapview.Marker>
                </Mapview>
                {/*<View style ={{flex:0.75,}}>*/}
                    {/*<ActionButton   buttonColor="white"*/}
                                    {/*renderIcon={() => <MaterialIcons name="my-location" size={30} color="blue"/>}*/}
                                    {/*onPress={this.onPress.bind(this)}*/}
                    {/*>*/}
                    {/*</ActionButton>*/}
                {/*</View>*/}

                <View style={styles.styleText1}>
                    <Text style={styles.Text1}>Vị trí hiện tại  </Text>
                    <Text style={styles.Text2}>TP. Đà Nẵng </Text>
                    <Text style={styles.Text3}>Đường Lương Nhữ Hộc </Text>

                </View>

                <View style={styles.general}>
                    <View style={styles.sectionRow1}>

                        <View style={styles.greenSpeed}>
                            <Image style={styles.greenIMG}
                                   source={require('./src/assets/images/green-speed_2018-09-06/green-speed.png')}>
                            </Image>
                            <Text style={styles.greenNumberSpeed}>65
                                <Text style={styles.greenTextSpeed}> KM/H</Text></Text>
                            <Text style={styles.styleText2}> Tốc độ hiện tại</Text>
                        </View>

                        <View style={styles.redSpeed}>
                            <Image style={styles.redIMG}
                                   source={require('./src/assets/images/red-one_2018-09-06/red-one.png')}>
                            </Image>
                            <Text style={styles.redNumberSpeed}>90
                                <Text style={styles.redTextSpeed}> KM/H</Text></Text>
                            <Text style={styles.styleText2}> Tốc độ cho phép</Text>
                        </View>

                    </View>

                    <View style={styles.sectionRow2}>

                        <View style={styles.sosSectionIMG}>
                            <Image style={styles.sosIMG}
                                   source={require('./src/assets/images/mess/icon.png')}>
                            </Image>
                        </View>

                        <TouchableOpacity style={{flex:1}}
                                          onPress={ () => {this.onPress1()}}>
                            <View style={styles.sosSectionText}>
                                <Text style={styles.styleText3}>Phát tín hiệu SOS</Text>
                            </View>
                        </TouchableOpacity>

                        <View style={styles.policeSectionIMG}>
                            <Image style={styles.policeIMG}
                                   source={require('./src/assets/images/police/icon.png')}>
                            </Image>
                        </View>

                        <TouchableOpacity style={{flex:1}}
                                          onPress={ () => {this.onPress2()}}>
                            <View style={styles.policeSectionText}>
                                <Text style={styles.styleText3}>Báo trạm Công An</Text>
                            </View>
                        </TouchableOpacity>

                    </View>

                    <View style={styles.sectionRow3}>

                        <View style={styles.findSectionIMG}>
                            <Image style={styles.findIMG}
                                   source={{uri:'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/compass-icon.png'}}>
                            </Image>
                        </View>

                        <TouchableOpacity style={{flex:1}}
                                          onPress={ () => {this.onPress3()}}>
                            <View style={styles.findSectionText}>
                                <Text style={styles.styleText3}>Tìm cây xăng</Text>
                            </View>
                        </TouchableOpacity>

                        <View style={styles.accountSectionIMG}>
                            <Image style={styles.accountIMG}
                                   source={require('./src/assets/images/human/icon.png')}>
                            </Image>
                        </View>

                        <TouchableOpacity style={{flex:1}}
                                          onPress={() => {this.onPress4()}}>
                            <View style={styles.accountSectionText}>
                                <Text style={styles.styleText3}>Tài khoản của bạn</Text>
                            </View>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>
        )
    }
}


