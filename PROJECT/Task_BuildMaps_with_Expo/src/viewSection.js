import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles';

const Text_location = () => {
    return(
    <View style={styles.styleText1}>
        <Text style={styles.Text1}>Vị trí hiện tại  </Text>
        <Text style={styles.Text2}>TP. Đà Nẵng </Text>
        <Text style={styles.Text3}>Đường Lương Nhữ Hộc </Text>
    </View>
    )
};

const View_greenSpeed = () => {
    return(
        <View style={styles.greenSpeed}>
            <Image style={styles.greenIMG}
                   source={require('./assets/images/green-speed_2018-09-06/green-speed.png')}>
            </Image>
            <Text style={styles.greenNumberSpeed}>65
                <Text style={styles.greenTextSpeed}> KM/H</Text></Text>
            <Text style={styles.styleText2}> Tốc độ hiện tại</Text>
        </View>
    )
};

const View_redSpeed = () => {
    return(
        <View style={styles.redSpeed}>
            <Image style={styles.redIMG}
                   source={require('./assets/images/red-one_2018-09-06/red-one.png')}>
            </Image>
            <Text style={styles.redNumberSpeed}>90
                <Text style={styles.redTextSpeed}> KM/H</Text></Text>
            <Text style={styles.styleText2}> Tốc độ cho phép</Text>
        </View>
    )
};

const View_SOS = () => {
    return(
        <View style={styles.sosSectionIMG}>
            <Image style={styles.sosIMG}
                   source={require('./assets/images/mess/icon.png')}>
            </Image>
        </View>
    )
};

const View_POLICE = () => {
    return(
        <View style={styles.policeSectionIMG}>
            <Image style={styles.policeIMG}
                   source={require('./assets/images/police/icon.png')}>
            </Image>
        </View>
    )
};

const View_FIND = () => {
    return(
        <View style={styles.findSectionIMG}>
            <Image style={styles.findIMG}
                   source={{uri:'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/compass-icon.png'}}>
            </Image>
        </View>
    )
};

const View_ACCOUNT = () => {
    return(
        <View style={styles.accountSectionIMG}>
            <Image style={styles.accountIMG}
                   source={require('./assets/images/human/icon.png')}>
            </Image>
        </View>
    )
};

export {Text_location, View_greenSpeed, View_redSpeed, View_SOS, View_POLICE, View_FIND, View_ACCOUNT,}