import React, {Component} from 'react';
import {Alert, Text, Image, View, TouchableOpacity, Dimensions} from 'react-native';
import Mapview, {PROVIDER_GOOGLE} from 'react-native-maps';
import styles from './src/styles';
import MapStyle from './src/MapStyle';
import {Button_SOS,Button_POLICE,Button_FIND,Button_ACCOUNT} from './src/buttonSection';
import {Text_location, View_greenSpeed, View_redSpeed, View_SOS, View_POLICE, View_FIND, View_ACCOUNT,} from "./src/viewSection";


let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 16.0361983;
const LONGITUDE = 108.2108347;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class App extends Component{

    constructor(props){
        super(props);
        this.state={
            region:{
                latitude:LATITUDE,
                longitude:LONGITUDE,
                latitudeDelta:LATITUDE_DELTA,
                longitudeDelta:LONGITUDE_DELTA
            },
            marker:{
                latitude:16.0202575,
                longitude:108.218597,
            },
        }
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    },
                    marker:{
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                });
            },
            (error) => console.log(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
        this.watchID = navigator.geolocation.watchPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
        );
    }
    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);}

    render(){
        return(
            <View style={styles.container}>
                <Mapview style={styles.map}
                         customMapStyle={MapStyle}
                         region={this.state.region}
                         showsUserLocation={true}
                         followsUserLocation={true}
                         provider={PROVIDER_GOOGLE}
                         //onRegionChange={ region => this.setState({region}) }
                    >
                    <Mapview.Marker
                        coordinate={this.state.marker}>
                        <View>
                            <Image source={require('./src/assets/images/car_2018-09-06/car.png')}></Image>
                        </View>
                    </Mapview.Marker>
                </Mapview>

                <Text_location/>

                <View style={styles.general}>

                    <View style={styles.sectionRow1}>
                        <View_greenSpeed/>
                        <View_redSpeed/>
                    </View>

                    <View style={styles.sectionRow2}>
                        <View_SOS/>
                        <Button_SOS/>
                        <View_POLICE/>
                        <Button_POLICE/>
                    </View>

                    <View style={styles.sectionRow3}>
                        <View_FIND/>
                        <Button_FIND/>
                        <View_ACCOUNT/>
                        <Button_ACCOUNT/>
                    </View>

                </View>
            </View>
        )
    }
}


