/** @format */

import {AppRegistry} from 'react-native';
import Test from './components/Test';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Test);
