import React, {Component} from 'react';
import {View} from 'react-native';
import styles from './styles';
import {Button_SOS,
        Button_POLICE,
        Button_FIND,
        Button_ACCOUNT} from './buttonSection';
import {Text_location,
        ViewSpeed,
        View_SOS,
        View_POLICE,
        View_FIND,
        View_ACCOUNT,} from './viewSection';
import View_mapviewMarker from './View_mapviewMarker';
import SplashScreen from "react-native-splash-screen";



export default class App extends Component{

    componentDidMount(){
        SplashScreen.hide();
    }

    render(){
        return(
            <View style={styles.container}>
                <View_mapviewMarker/>

                <Text_location/>

                <View style={styles.general}>

                    <View style={styles.sectionRow1}>
                    <ViewSpeed
                            imageUrl={require('./assets/images/green-speed_2018-09-06/green-speed.png')}
                            speed={40}
                            speedTitle='Tốc độ cho phép'
                            customColor='green'
                            numberColor='green'
                        />
                    <ViewSpeed
                            border={18}
                            imageUrl={require('./assets/images/red-one_2018-09-06/red-one.png')}
                            speed={65}
                            speedTitle='Tốc độ tối đa'
                            customColor='red'
                            numberColor='red'
                        />
                    </View>

                    <View style={styles.sectionRow2}>
                        <View_SOS/>
                        <Button_SOS/>
                        <View_POLICE/>
                        <Button_POLICE/>
                    </View>

                    <View style={styles.sectionRow3}>
                        <View_FIND/>
                        <Button_FIND/>
                        <View_ACCOUNT/>
                        <Button_ACCOUNT/>
                    </View>

                </View>
            </View>
        )
    }
}


