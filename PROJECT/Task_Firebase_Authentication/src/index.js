import React, { Component } from 'react';
import {View, Text, TextInput, Image, ImageBackground, TouchableOpacity, Alert, Platform,} from 'react-native';
import { Navigator} from 'react-native-deprecated-custom-components';
import SplashScreen from 'react-native-splash-screen';
import LoginGoogleFacebook from './loginGoogleFacebook';
import styles from './styles';
import Login from './Login';
import Register from './Register';
import Welcome from './Welcome';

export default class App extends Component {

    renderScene(route, Navigator){
        switch (route.name) {
            case 'login': return(
                <Login
                    goRegister={()=>{
                        Navigator.push({name:'register'})
                    }}
                    goWelcome={()=>{
                        Navigator.push({name:'welcome'})
                    }}
                />
            );
            case 'register': return(
                <Register
                    goLogin={()=> {
                        Navigator.push({name:'login'})
                    }}
                 />
            );
            case 'welcome': return(
                <Welcome/>
            );
        }
    }

    render() {
        return(
                <Navigator initialRoute={{name:'login'}}    // Ban đầu vào sẽ nhảy đến màn hình login
                           renderScene={this.renderScene}/>
        );
    }
}

