import React, {Component} from 'react';
import FBSDK, {LoginManager, LoginButton, AccessToken} from 'react-native-fbsdk';
import {TouchableOpacity, Text, View} from 'react-native';
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
import firebase from 'react-native-firebase';
GoogleSignin.configure();
export default class LoginGoogleFacebook extends Component {

    componentWillMount() {
        GoogleSignin.hasPlayServices({showPlayServicesUpdateDialog: true});
        GoogleSignin.configure({
            webClientId: "189437886526-trc7p7meq7c72a592u25lu1bcs770fpr.apps.googleusercontent.com",
            //offlineAccess: true,
            forceConsentPrompt: true,
        })
    }

    googleAuth = () => {
        GoogleSignin
        .signIn()
        .then((data) => {
            // create a new firebase credential with the token
            const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
            const userlogin = firebase.auth().signInWithCredential(credential);
            console.log(userlogin);
            // login with credential
            return userlogin;
        })
        .then((user) => {
            //console.log(`Google Login with user : ${JSON.stringify(user.toJSON())}`);
            console.log(user);
        })
        .catch((error) => {
            console.log(`Login fail with error: ${error}`);
        });
}


    fbAuth = async () => {
        // try {
        //     const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
        //     const tokenData = await AccessToken.getCurrentAccessToken();
        //     const token = tokenData.accessToken.toString();
        //     const credential = firebase.auth.FacebookAuthProvider.credential(token);
        //     const user = await firebase.auth().signInWithCredential(credential);
        //     console.log(user);
        // }
        // catch (error){
        //     console.log(error.message);
        // }
    }

    render() {
        return (
            <View>
                <LoginButton
                     onPress={this.fbAuth}/>  
                <GoogleSigninButton
                    style={{ flex:0.7}}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={this.googleAuth}/>
            </View>

        );
    }
}