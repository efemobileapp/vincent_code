import React, {Component} from 'react';
import {View, Text, ImageBackground, Image, TextInput, TouchableOpacity, Alert, Platform, Keyboard} from 'react-native';
import styles from "./styles";
import LoginGoogleFacebook from "./loginGoogleFacebook";
import SplashScreen from "react-native-splash-screen";
import firebase from 'react-native-firebase';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import {DismissKeyboard} from './dismissKeyboard';

const background = {uri:'https://t1.daumcdn.net/cfile/tistory/24694950591EBAD421'};
const user = require("./assets/images/screen/User.png");
const lock = require("./assets/images/screen/Lock.png");
const logo = {uri:'https://pre00.deviantart.net/239a/th/pre/i/2014/106/6/c/random_logo_by_criticl-d7eqdvw.png'};

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
        'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});



export default class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password:'',
        }
    }

    state = {
        logged: false
    }
    
    componentDidMount(){
        SplashScreen.hide();
    }

    DangNhap = () => {
        firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(()=>{
                Alert.alert(
                    'Đăng nhập thành công',
                    'Xin vui lòng chờ!',
                    [
                        {text: 'Cancel', onPress: () => { this.props.goWelcome(); Keyboard.dismiss() }},
                        {text: 'OK', onPress: () => { this.props.goWelcome(); Keyboard.dismiss() }},
                    ],
                    { cancelable: false }
                );
                this.setState({
                    email:'',
                    password:'',
                })
            })
        .catch(function(error) {
                Alert.alert(
                    'Thông báo',
                    'Đăng nhập thất bại! Hãy thử lại!',
                    [
                        {text: 'Cancel', onPress: () => {Keyboard.dismiss() }},
                        {text: 'OK', onPress: () => {Keyboard.dismiss() }},
                    ],
                    { cancelable: false }
                );
            });
    }
    onPress7 = async () => {
        try {
            const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
            const tokenData = await AccessToken.getCurrentAccessToken();
            const token = tokenData.accessToken.toString();
            const credential = firebase.auth.FacebookAuthProvider.credential(token);
            const user = await firebase.auth().signInAndRetrieveDataWithCredential(credential);
            console.log(user);
        }
        catch (error){
            console.log(error.message);
        }
    }
    render(){
        return(
            
            <ImageBackground source={background}
                             resizeMode="cover"
                             style={[styles.screenContainer, styles.screenBackground]}
            >
            
                <Image source={logo}
                       style={{flex:0.5}}/>   
            <DismissKeyboard>       
                <View style = {styles.wrapper}>

                    <View style = {styles.inputWrap}>
                    
                        <View style = {styles.iconWrap}>
                            <Image source={user}
                                   style={styles.icon}
                                   resizeMode="contain"/>
                        </View>
                        <TextInput placeholder="Tên đăng nhập"
                                   style = {styles.input}
                                   underlineColorAndroid="transparent"
                                   onChangeText={(email) => this.setState({email})}
                                   value={this.state.email}/>
                    </View>

                    <View style = {styles.inputWrap}>
                        <View style = {styles.iconWrap}>
                            <Image source={lock}
                                   style={styles.icon}
                                   resizeMode="contain"/>
                        </View>
                        <TextInput placeholder="********"
                                   style = {styles.input}
                                   underlineColorAndroid="transparent"
                                   secureTextEntry={true}
                                   onChangeText={(password)=>this.setState({password})}
                                   value={this.state.password}/>
                    </View>

                </View>
            </DismissKeyboard>

            <DismissKeyboard>
                <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={{
                        flex:0.5,
                        backgroundColor:'cyan',
                        borderRadius:18,
                        borderWidth:2.5,
                        width:100, height:40,
                        justifyContent:'center',
                        alignItems: 'center',
                        margin:40}}
                        onPress={this.props.goRegister}  >
                        <Text style ={{fontWeight:'bold', fontSize:14}}> ĐĂNG KÝ</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{
                        flex:0.5,
                        backgroundColor:'cyan',
                        borderRadius:18,
                        borderWidth:2.5,
                        width:100, height:40,
                        justifyContent:'center',
                        alignItems: 'center',
                        margin:40}}
                        onPress={this.DangNhap} >
                        <Text style ={{fontWeight:'bold', fontSize:14}}> ĐĂNG NHẬP</Text>
                    </TouchableOpacity>
                </View>
            </DismissKeyboard>

            <DismissKeyboard>
                <View   style ={{flex: 0.25,
                        justifyContent: 'center',
                        alignItems: 'center',}}>
                    <TouchableOpacity style = {{alignItems:'center'}}>
                        <Text style={{alignItems:'center'}}>Quên mật khẩu?</Text>
                    </TouchableOpacity>
                </View>
            </DismissKeyboard>

            <DismissKeyboard>
                <View   style ={{flex: 0.25,
                        justifyContent: 'center',
                        alignItems: 'center' }}>
                    <TouchableOpacity style = {{backgroundColor:'b00000ff',
                                                borderRadius:18}}
                                      onPress={this.onPress7}>
                        <Text style ={{fontWeight:'bold'}}>{this.state.logged ? 'Đăng xuất' : 'Đăng nhập'}</Text>              
                    </TouchableOpacity>
                    <LoginGoogleFacebook/>
                </View>
            </DismissKeyboard>

            </ImageBackground>   
            
        );
    }
}