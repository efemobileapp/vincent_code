import React, {Component} from 'react';
import {View, Text, ImageBackground, Image, TextInput, TouchableOpacity, Alert, Keyboard} from 'react-native';
import styles from "./styles";
import firebase from "react-native-firebase";
import DismissKeyboard from './dismissKeyboard';


const background = {uri:'https://t1.daumcdn.net/cfile/tistory/24694950591EBAD421'};
const user = require("./assets/images/screen/User.png");
const lock = require("./assets/images/screen/Lock.png");
const logo = {uri:'https://pre00.deviantart.net/239a/th/pre/i/2014/106/6/c/random_logo_by_criticl-d7eqdvw.png'};

export default class Register extends Component{
        constructor(props){
            super(props);
            this.state = {
                email:'',
                password:'',
            }
        }

        Dangky = () => {
            firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(()=>{
                    Alert.alert(
                        'Xác nhận',
                        'Đăng ký thành công! Cảm ơn bạn!',
                        [
                            {text: 'OK', onPress: () => {this.props.goLogin(); Keyboard.dismiss() }},
                        ],
                        { cancelable: false }
                    );
                    this.setState({
                        email:'',
                        password:'',
                    })
                })
            .catch(function(error) {
                    Alert.alert(
                        'Thông báo',
                        'Đăng ký thất bại! Hãy thử lại!',
                        [
                            {text: 'Cancel', onPress: () => {Keyboard.dismiss()}},
                            {text: 'OK', onPress: () => {Keyboard.dismiss()}},
                        ],
                        { cancelable: false }
                    );
                });
        }
        render(){
            return(
                <ImageBackground source={background}
                                 resizeMode="cover"
                                 style={[styles.screenContainer, styles.screenBackground]}
                >
                
                <Image source={logo}
                           style={{flex:0.5}}/>

                <DismissKeyboard> 
                    <View style = {styles.wrapper}>
                        <View style = {styles.inputWrap}>
                            <View style = {styles.iconWrap}>
                                <Image source={user}
                                       style={styles.icon}
                                       resizeMode="contain"/>
                            </View>
                            <TextInput placeholder="Username"
                                       style = {styles.input}
                                       underlineColorAndroid="transparent"
                                       onChangeText={(email) => this.setState({email})}
                                       value={this.state.email}/>
                        </View>
                        <View style = {styles.inputWrap}>
                            <View style = {styles.iconWrap}>
                                <Image source={lock}
                                       style={styles.icon}
                                       resizeMode="contain"/>
                            </View>
                            <TextInput placeholder="********"
                                       style = {styles.input}
                                       underlineColorAndroid="transparent"
                                       secureTextEntry={true}
                                       onChangeText={(password)=>this.setState({password})}
                                       value={this.state.password}/>
                        </View>
                    </View>
                </DismissKeyboard>

                <DismissKeyboard>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity style={{
                            flex:1,
                            backgroundColor:'cyan',
                            borderRadius:18,
                            borderWidth:2.5,
                            width:100, height:40,
                            justifyContent:'center',
                            alignItems: 'center',
                            margin:40}}
                            onPress={this.Dangky}>
                            <Text style ={{fontWeight:'bold', fontSize:14}}> ĐỒNG Ý CÁC ĐIỀU KHOẢN VÀ ĐĂNG KÝ</Text>
                        </TouchableOpacity>
                    </View>
                </DismissKeyboard>

                </ImageBackground>
            );
        }
    }