import React, {Component} from 'react';
import FBSDK, {LoginManager, LoginButton, AccessToken} from 'react-native-fbsdk';
import {TouchableOpacity, Text, View} from 'react-native';
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';

export default class LogIn extends Component {

    async _setupGoogleSignin() {
        try {
          await GoogleSignin.hasPlayServices({ autoResolve: true });
          await GoogleSignin.configure({
            webClientId: '189437886526-trc7p7meq7c72a592u25lu1bcs770fpr.apps.googleusercontent.com',
            offlineAccess: false
          });
    
          const user = await GoogleSignin.currentUserAsync();
          console.log(user);
          this.setState({user});
        }
        catch(err) {
          console.log("Play services error", err.code, err.message);
        }
      }

    googleAuth() {
        GoogleSignin.signIn().then((user) => {
            console.log(user);
            this.setState({user: user});
            console.log(user.token+" token");
            const credential = {
                provider: 'google',
                token: user.accessToken,
                secret: user.id, //I do not know what to send in secret
            };
            Authentication.googleLogin(credential);
        })
            .catch((err) => {
                alert('WRONG SIGNIN'+ err);
            })
            .done();
    }

    googleAuth = () => {
        GoogleSignin
        .signIn()
            .then((data) => {
                // create a new firebase credential with the token
                const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
                // login with credential
                return firebase.auth().signInWithCredential(credential);
            })
            .then((currentUser) => {
                console.log(`Google Login with user : ${JSON.stringify(currentUser.toJSON())}`);
            })
            .catch((error) => {
                console.log(`Login fail with error: ${error}`);
            });
    }

    fbAuth = () => {
        LoginManager
        .logInWithReadPermissions(['public_profile', 'email'])
        .then((result) => {
            if (result.isCancelled) {
                return Promise.reject(new Error('The user cancelled the request'));
            }
            console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
            // get the access token
            return AccessToken.getCurrentAccessToken();
        })
        .then(data => {
            const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
            return firebase.auth().signInWithCredential(credential);
        })
        .then((currentUser) => {
            console.log(`Facebook Login with user : ${JSON.stringify(currentUser.toJSON())}`);
        })
        .catch((error) => {
            console.log(`Facebook login fail with error: ${error}`);
        });
}
    
    render() {
        return (
            <View>
                <LoginButton
                    onPress = {this.fbAuth}
                    />


                <TouchableOpacity onPress={this.googleAuth.bind(this)}>
                    <Text>Login with Google</Text>
                </TouchableOpacity>
            </View>

        );
    }
}