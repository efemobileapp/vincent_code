import React, {Component} from 'react';
import {Alert, Text, Image, View, TouchableOpacity, Dimensions} from 'react-native';
import Mapview, {PROVIDER_GOOGLE} from 'react-native-maps';
import styles from './src/styles';
import MapStyle from './src/MapStyle';
import {Button_SOS,Button_POLICE,Button_FIND,Button_ACCOUNT} from './src/buttonSection';
import {Text_location, View_SOS, View_POLICE, View_FIND, View_ACCOUNT, ViewSpeed} from "./src/viewSection";
import View_mapviewMarker from './src/View_mapviewMarker';

let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 16.0361983;
const LONGITUDE = 108.2108347;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class App extends Component{

    render(){
        return(
            <View style={styles.container}>
                <View_mapviewMarker/>

                <Text_location/>

                <View style={styles.general}>

                    <View style={styles.sectionRow1}>
                        <ViewSpeed
                        imageUrl={require('./assets/images/green-speed_2018-09-06/green-speed.png')}
                        speed={40}
                        speedTitle='Tốc độ cho phép'
                        customColor='green'
                        numberColor='green'
                        /> 
                        <ViewSpeed
                        border={18}
                        imageUrl={require('./assets/images/red-one_2018-09-06/red-one.png')}
                        speed={65}
                        speedTitle='Tốc độ tối đa'
                        customColor='red'
                        numberColor='red'
                        />
                    </View>

                    <View style={styles.sectionRow2}>
                        <View_SOS/>
                        <Button_SOS/>
                        <View_POLICE/>
                        <Button_POLICE/>
                    </View>

                    <View style={styles.sectionRow3}>
                        <View_FIND/>
                        <Button_FIND/>
                        <View_ACCOUNT/>
                        <Button_ACCOUNT/>
                    </View>

                </View>
            </View>
        )
    }
}


