import React from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles';

const Text_location = () => {
    return(
    <View style={styles.styleText1}>
        <Text style={styles.Text1}>Vị trí hiện tại  </Text>
        <Text style={styles.Text2}>TP. Đà Nẵng </Text>
        <Text style={styles.Text3}>Đường Lương Nhữ Hộc </Text>
    </View>
    )
};

const ViewSpeed = ({border, imageUrl, numberColor, speed, customColor, speedTitle}) => {
    return (
        <View style={[styles.Speed, {borderTopRightRadius:border}]}>
            <Image style={styles.greenIMG}
                    source={imageUrl}>
            </Image>
            <Text style={[styles.numberSpeed, {color: numberColor}]}>{speed}
                <Text style={[styles.textSpeed, {color : customColor}]}> KM/H</Text></Text>
            <Text style={styles.styleText2}> {speedTitle}</Text>
        </View>
    );
};

const View_SOS = () => {
    return(
        <View style={styles.sosSectionIMG}>
            <Image style={styles.sosIMG}
                   source={require('./assets/images/mess/icon.png')}>
            </Image>
        </View>
    )
};

const View_POLICE = () => {
    return(
        <View style={styles.policeSectionIMG}>
            <Image style={styles.policeIMG}
                   source={require('./assets/images/police/icon.png')}>
            </Image>
        </View>
    )
};

const View_FIND = () => {
    return(
        <View style={styles.findSectionIMG}>
            <Image style={styles.findIMG}
                   source={{uri:'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/compass-icon.png'}}>
            </Image>
        </View>
    )
};

const View_ACCOUNT = () => {
    return(
        <View style={styles.accountSectionIMG}>
            <Image style={styles.accountIMG}
                   source={require('./assets/images/human/icon.png')}>
            </Image>
        </View>
    )
};

export {Text_location, View_SOS, View_POLICE, View_FIND, View_ACCOUNT, ViewSpeed}