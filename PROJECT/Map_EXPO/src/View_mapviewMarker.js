import {Component} from "react";
import {Dimensions, Image, View} from "react-native";
import styles from "./styles";
import MapStyle from "./MapStyle";
import React from "react";
import Mapview, {PROVIDER_GOOGLE} from 'react-native-maps';
import { Constants, Location, Permissions, MapView } from 'expo';

let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 16.0361983;
const LONGITUDE = 108.2108347;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class View_mapviewMarker extends Component {
        state = {
            location: null,
            errorMessage: null,
            };
    _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }
        let location = await Location.getCurrentPositionAsync({});
        console.log(location);
        this.setState({ location });
    };


    // componentDidMount() {
    //     this._getLocationAsync();
    //     navigator.geolocation.getCurrentPosition(
    //         position => {
    //             console.log(position);
    //             this.setState({
    //                 region: {
    //                     latitude: position.coords.latitude,
    //                     longitude: position.coords.longitude,
    //                     latitudeDelta: LATITUDE_DELTA,
    //                     longitudeDelta: LONGITUDE_DELTA,
    //                 },
    //                 marker: {
    //                     latitude: position.coords.latitude,
    //                     longitude: position.coords.longitude,
    //                 }
    //             });
    //         },
    //         (error) => console.log(error.message),
    //         {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    //     );
    //     this.watchID = navigator.geolocation.watchPosition(
    //         (position) => {
    //             this.setState({
    //                 latitude: position.coords.latitude,
    //                 longitude: position.coords.longitude,
    //                 error: null,
    //             });
    //         },
    //         (error) => this.setState({error: error.message}),
    //         {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10},
    //     );
    // }

    componentWillMount() {
    this._getLocationAsync()
    }

    render() {
        let text = 'Waiting..';
        if (this.state.errorMessage) {
            text = this.state.errorMessage;
        } else if (this.state.location) {
            text = JSON.stringify(this.state.location);
        }
        return (
            <MapView
            style={styles.map}
            customMapStyle={MapStyle}
            showsUserLocation={true}
            followsUserLocation={true}
            provider={PROVIDER_GOOGLE}
            />
        )
    }
}